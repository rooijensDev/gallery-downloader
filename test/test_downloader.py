import unittest

from downloader import main
from downloader import Downloader

class DownloaderTest(unittest.TestCase):

    def setUp(self):
        self.args = main([
            "https://www.codelikethewind.org/content/images/size/w2000/2022/05/hello_world.png",
            "--dir", "Example Directory"
            ])
    def test_print_args(self):
        self.assertTrue(self.args)

        print(self.args)
        print(f'url: {self.args.url}')
        print(f'dir: {self.args.dir}')

    def test_download_single_image(self):
       Downloader.download_single_image(self.args.url)


if __name__ == '__main__':
    unittest.main()