import argparse
import sys
import requests
class Downloader:
    def parse_args(args):
        parser = argparse.ArgumentParser(description='Creates a PDF of a gallery by passing the first image url')
        parser.add_argument('url', metavar='url', type=str,
                            help='the url of the first image, e.g. https://x.com/galleries/x/1.jpg')

        parser.add_argument('--dir', metavar='folder-path', type=str,
                            help='Instead of making a PDF, store all images separately in given folder')
        return parser.parse_args(args)

    def download_single_image(url):
        img_data = requests.get(url).content
        with open('test_download_single_image.jpg', 'wb') as handler:
            handler.write(img_data)

def main(argv=None):
    args = Downloader.parse_args(argv)
    return args

if __name__ == '__main__':
    sys.exit(main())